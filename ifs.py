age = 23
if age > 17:
    print("You can buy a lottery ticket.")
    print("How many would you like?")
else:
    print("You may not buy a lottery ticket.")
    print("Can I interest you in some candy?")

print("Thank you for your patronage.")

x = 7

if x < 10:
    print("ABC")
if x < 8:
    print("LMN")
if x < 4:
    print("XYZ")

days = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
]
days[2] = "Bluesday"
print(days)